//
// Created by msd on 6/27/18.
//

#ifndef SFMLPONG_GAME_H
#define SFMLPONG_GAME_H

#include "ball.h"

class Game
{
public:
    Game(int width, int height, int p_width);
    void move();
    void move_player(int p, char way);
    int get_width();
    int get_height();
    int get_scores(int player);
    int get_positions(int player);
    void set_position(int playerIndex, float value);
    int get_player_width();
    Ball *get_ball();

private:
    int width, height, scores[2], p_width;
    float p_positions[2];
    Ball *ball;
};

#endif //SFMLPONG_GAME_H
