//
// Created by msd on 6/27/18.
//

#ifndef SFMLPONG_RENDERER_H
#define SFMLPONG_RENDERER_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "game.h"

void renderer(sf::RenderWindow *window, Game *game)
{
    Ball *ball = game->get_ball();
    sf::CircleShape shape;
    shape.setRadius(5);
    shape.setPosition(ball->get_x(), ball->get_y());
    shape.setOrigin(5, 5);
    shape.setFillColor(sf::Color::Cyan);

    sf::RectangleShape p1(sf::Vector2f(3, game->get_player_width()));
    p1.setPosition(0,game->get_positions(0));

    sf::RectangleShape p2(sf::Vector2f(3, game->get_player_width()));
    p2.setPosition(game->get_width()-3,game->get_positions(1));

    sf::Text scores;
    std::string s = "0:0";
    s[0] += game->get_scores(0);
    s[2] += game->get_scores(1);

//    scores.setString(s);
//    scores.setPosition(game->get_width()/2, 10);
//    scores.setCharacterSize(24);
//    scores.setFillColor(sf::Color::White);
//
//    window->draw(scores);
    window->draw(p1);
    window->draw(p2);
    window->draw(shape);
}

#endif //SFMLPONG_RENDERER_H
