//
// Created by msd on 6/27/18.
//

#ifndef SFMLPONG_BALL_H
#define SFMLPONG_BALL_H

#include <math.h>

class Ball
{
public:
    Ball(float x, float y, float dx, float dy, float speed);
    float get_x();
    float get_y();
    void set_x(float x);
    void set_y(float y);
    void reverse_dy();
    void reverse_dx();
    void move();
private:
    float x, y, dx, dy, speed;
};

#endif //SFMLPONG_BALL_H
