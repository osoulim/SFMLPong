#include "headers/game.h"
#include "headers/renderer.h"
#include "headers/network.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <thread>
#include <chrono>

int main()
{
    int a;
    std::cin >> a;

    Game *game = new Game(640, 480, 160);
    Resource* resource = new Resource(game);
    Network* network;
    if (a == 0){
        network = new Network(resource);
        network->listen();
    } else {
        network = new Network(resource, "127.0.0.1");
        network->connect();
    }

    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML Application");
    sf::CircleShape shape;
    shape.setRadius(40.f);
    shape.setPosition(100.f, 100.f);
    shape.setFillColor(sf::Color::Cyan);
    sf::Event event;
    while (window.isOpen())
    {
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed) {

                if (event.key.code == sf::Keyboard::Up)
                    game->move_player(1, 'U');
                if (event.key.code == sf::Keyboard::Down)
                    game->move_player(1, 'D');

                if (event.key.code == sf::Keyboard::W)
                    game->move_player(0, 'U');
                if (event.key.code == sf::Keyboard::S)
                    game->move_player(0, 'D');
            }
        }
        game->move();

        network->send();
        network->receive();
        std::this_thread::sleep_for (std::chrono::milliseconds(40));

        window.clear();
        renderer(&window, game);
        window.display();
    }
}