//
// Created by msd on 6/27/18.
//

#include "../headers/ball.h"

Ball::Ball(float x, float y, float dx, float dy, float speed) {
    this->x = x;
    this->y = y;
    this->dx = dx;
    this->dy = dy;
    this->speed = speed;
}

float Ball::get_x() {
    return this->x;
}

float Ball::get_y() {
    return this->y;
}

void Ball::reverse_dy() {
    this->dy = - this->dy;
}

void Ball::reverse_dx() {
    this->dx = - this->dx;
}

void Ball::move() {
    this->x += this->dx * speed;
    this->y += this->dy * speed;
}

void Ball::set_x(float x) {
    this->x = x;
}

void Ball::set_y(float y) {
    this->y = y;
}
