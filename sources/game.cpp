//
// Created by msd on 6/27/18.
//

#include "../headers/game.h"
#include <algorithm>

Game::Game(int width, int height, int p_width) {
    this->width = width;
    this->height = height;
    if(p_width < height)
        this->p_width = p_width;
    else
        this->p_width = height/3;
    p_positions[0] = p_positions[1] = height/2 - p_width/2;
    scores[0] = scores[1] = 0;
    ball = new Ball(width/2, height/2, 1/pow(2, 0.5), 1/pow(2, 0.5), 0.7);
}

int Game::get_width() {
    return this->width;
}

int Game::get_height() {
    return this->height;
}

int Game::get_scores(int player) {
    return this->scores[player];
}

int Game::get_positions(int player) {
    return this->p_positions[player];
}

int Game::get_player_width() {
    return this->p_width;
}

Ball* Game::get_ball() {
    return this->ball;
}

void Game::move() {
    this->ball->move();
    float ball_x = this->ball->get_x(), ball_y = this->ball->get_y();

    if(ball_y <= 0 || ball_y >= this->height)
        this->ball->reverse_dy();

    if(ball_x <= 0 && (ball_y < this->p_positions[0] || ball_y > this->p_positions[0] + this->p_width))
    {
        this->ball->set_x(this->width/2);
        this->ball->set_y(this->height/2);
        this->scores[1]++;
    }
    else if(ball_x <= 0)
        this->ball->reverse_dx();


    if(ball_x >= this->width  && (ball_y < this->p_positions[1] || ball_y > this->p_positions[1] + this->p_width))
    {
        this->ball->set_x(this->width/2);
        this->ball->set_y(this->height/2);
        this->scores[0]++;
    }
    else if(ball_x >= this->width)
        this->ball->reverse_dx();

}

void Game::move_player(int p, char way) {
    float speed = 10;
    if(way == 'U')
        this->p_positions[p] = std::max(0.0f, this->p_positions[p] - speed);

    if(way == 'D')
        this->p_positions[p] = std::min((float) this->height - this->p_width, this->p_positions[p] + speed);

}

void Game::set_position(int playerIndex, float value) {
    this->p_positions[playerIndex] = value;
}

